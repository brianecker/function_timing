import unittest
from time import sleep

from function_timing.timed_function import Timed


class FunctionTimingTests(unittest.TestCase):
    def __init__(self, *args, **kwargs):
        super(FunctionTimingTests, self).__init__(*args, **kwargs)
        self.epsilon = 0.1
        self.n = 5

    def test_no_time(self):
        @Timed
        def never_called():
            pass

        # Make sure this doesn't hang
        Timed.get_time(never_called)

    @Timed
    def m1(self, sleep_time):
        sleep(sleep_time)

    def test_function_timing(self):
        print("Doing function timing test, this will take a few seconds")

        @Timed
        def f1():
            sleep(1)

        @Timed
        def f2():
            sleep(0.5)

        # Run each ten times
        for _ in range(self.n):
            f1()
            f2()

        t1 = self.n * 1
        t2 = self.n * 0.5
        self.assertTrue((t1 - self.epsilon < Timed.get_time(f1)) and (Timed.get_time(f1) < t1 + self.epsilon))
        self.assertTrue((t2 - self.epsilon < Timed.get_time(f2)) and (Timed.get_time(f2) < t2 + self.epsilon))

    def test_method_timing(self):
        print("Doing method timing test, this will take a few seconds")
        # Run method m1 ten times
        for _ in range(self.n):
            self.m1(0.5)

        t = self.n * 0.5
        self.assertTrue((t - self.epsilon < Timed.get_time("m1")) and (Timed.get_time("m1") < t + self.epsilon))

    def test_get_times(self):
        times = Timed.get_times()
        times.pop("m1")
        self.assertTrue("m1" in Timed._timed_funcs)

    def test_get_tracked(self):
        tracked = Timed.get_tracked()
        tracked.remove("m1")
        self.assertTrue("m1" in Timed._timed_funcs)


