try:
    from setuptools import setup, find_packages
except ImportError:
    from distutils.core import setup, find_packages


config ={
    "description": "Provides decorators for timing individual functions and methods each time they are run. Will provide other timing utilities in the future.""",
    "author": "Brian Ecker",
    "license": "MIT",
    "url": "https://bitbucket.org/brianecker/function_timing/overview",
    "download_url": "",
    "author_email": "briancecker@gmail.com",
    "version": "0.1",
    "install_requires": [],
    "packages": find_packages(),
    "scripts": [],
    "name": "function-timing",
    "test_suite": "tests"
}

setup(**config)
