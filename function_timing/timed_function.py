import time

from multiprocessing import JoinableQueue
# from multiprocessing import Queue
from queue import Empty


class Timed:
    """
    A decorator class that keeps track of the total running time of a function
    or a method. The time can be retrieved with Timed.get_time.
    """
    _timed_funcs = dict()

    def __init__(self, func):
        self.func = func
        for name in set(dir(func)) - set(dir(self)):
            setattr(self, name, getattr(func, name))
        Timed._timed_funcs[self.func.__name__] = JoinableQueue()

    def __get__(self, obj, objtype):
        import functools
        return functools.partial(self.__call__, obj)

    def __call__(self, *args):
        start = time.time()
        result = self.func(*args)
        end = time.time()
        Timed._timed_funcs[self.func.__name__].put(end - start)
        return result

    @staticmethod
    def _sum_queue(queue):
        # Block the queue while we sum it
        time_sum = 0.0
        while True:
            try:
                time_sum += queue.get(block=False)
            except Empty:
                break

        # Put the total into the queue
        queue.put(time_sum)
        return time_sum

    @staticmethod
    def get_time(func):
        """
        Retrieve the total time a registered function or method has run.
        :param func: Either a string that is the function or method name,
        or a function itself. Note that you cannot pass a method, and must
        pass the method name instead.
        :return: The total running time for func
        """
        print("?")

        if isinstance(func, str):
            func_name = func
        elif hasattr(func, "__name__"):
            func_name = func.__name__
        else:
            raise Exception("func argument must be a string or have a __name__ property")

        if func_name not in Timed._timed_funcs:
            raise Exception("{0} has not yet been timed".format(func_name))
        else:
            return Timed._sum_queue(Timed._timed_funcs[func_name])

    @staticmethod
    def get_times():
        """
        :return: Returns a dictionary of {"function_name", time, ... } for
        all tracked functions and methods.
        """
        return {func: Timed._sum_queue(q) for func, q in Timed._timed_funcs.items()}

    @staticmethod
    def get_tracked():
        """
        Retrive a set of tracked function and method names.
        :return: A set of tracked function and method names.
        """
        return set(Timed._timed_funcs.keys())

