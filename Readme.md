# Description
A python module providing a decorator that keeps track of the total running
time of any decorated function or method by name or function reference.
This makes timing various functions and methods easy. Simply decorate all
the functions you want to time with `@Timed` and retrieve the total run time
at any moment with `Timed.get_time("function_name")` or `Timed.get_time(function)`.


# Timing a function:
    from function_timing import Timed
    from time import sleep

    @Timed
    def f():
        sleep(1)

    for _ in range(5):
        f()

    print(Timed.get_time(f)) # Prints 5.00480842590332
    print(Timed.get_time("f")) # Also prints 5.00480842590332


# Timing a method:
    from function_timing import Timed
    from time import sleep

    class MyClass:
        @Timed
        def my_method(self):
            sleep(1)

    my_class = MyClass()
    for _ in range(5):
        my_class.my_method()
    # Note that Timed.get_time(MyClass.my_method) will NOT work here.
    print(Timed.get_time("my_method")) # Prints 5.005416393280029


